class BooksController < ApplicationController
    before_action :find_book, only: [:show, :edit, :update, :destroy]
    before_action :store_editor_name, only: [:create, :update]

    def index
        @books = Book.page(params[:page])
    end
    
    def show
    end

    def new
        @book = Book.new
    end

    def edit
    end

    def create
        
        @book = Book.new(book_params)
        if @book.save
            flash[:notice] = "Successfully created"
            redirect_to books_path
        else 
            render :new
        end
    end

    def update
        store_editor_name
        if @book.update(book_params)
            flash[:notice] = "Successfully updated"
            redirect_to books_path
        else
            render :edit
        end
    end

    def destroy
        @book.destroy
        flash[:alert] = "Successfully destroyed"
        redirect_to books_path
    end

    private

    def book_params
        params.require(:book).permit(:title, :author, :description, :pages_count, :published_at, :editor)
    end

    def find_book
        @book = Book.find(params[:id])
    end

    def store_editor_name
        session[:editor] = book_params[:editor]
    end
end
